#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"

import pandas as pd

def count_genes_per_variant(file):
    """
    Function to count genes per variant
    :param file: the file with all the variants and their genes
    :return: excel file with count of genes for each variant
    """
    df = pd.read_excel(file)  # put data in dataframe
    df = df.iloc[:, [2, 6]]  # select columns with variants and genes
    df = df.drop_duplicates()  # drop duplicates
    df = df['rsid'].value_counts() #count genes per variant

    # export into excel
    data_to_excel = pd.ExcelWriter('count_genes_per_variant.xlsx')
    df.to_excel(data_to_excel)
    data_to_excel.save()

    print("Your excel in now available")

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Lauréline Pierre"


import pandas as pd


def count_transcription_factors(file):
    """
    Function to count the number of transcription factors for a variant
    :param file: The file is the outout of the RSAT scan-matrix
    :return: excel with the count of each transcriptor factors
    """
    df = pd.read_csv(file, sep='\t')  # Import file in a dataframe
    df = df.iloc[:, [0, 2]]  # Conserve column with variant and transcriptor factor
    df = df[df["ft_name"].str.contains("START_END") == False]  # erase rows with the start
    df = df.drop_duplicates(keep=False)  # drop duplicates
    df = df['ft_name'].value_counts()  # count number of transcription factors

    # export into excel
    data_to_excel = pd.ExcelWriter('count_transcription_factors.xlsx')
    df.to_excel(data_to_excel)
    data_to_excel.save()

    print("Your excel in now available")



